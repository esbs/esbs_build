#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2021 EVBox B.V.
# Copyright (C) 2021 Olliver Schinagl <oliver@schinagl.nl>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%'shunit2'}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%"${1##*'/'}"}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../${1##*'/'}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

set +eu


verify_itb()
{
	_itb="${1:?Missing argument to function}"
	_image_nodes="${2:?Missing argument to function}"
	_config_nodes="${3:?Missing argument to function}"

	if [ ! -f "${_itb}" ]; then
		fail "No file to verify '${_itb}'"
		return
	fi

	echo "Verifying itb '${_itb}'"
	dumpimage -l "${_itb}"

	_config_cnt="$(dumpimage -l "${_itb}" | \
	               sed -n 's|[[:space:]]\+Configuration[[:space:]]\+\([[:digit:]]\+\).*|\1|p' | \
	               sort -n | \
	               tail -n 1)"

	_image_cnt="$(dumpimage -l "${_itb}" | \
	              sed -n 's|[[:space:]]\+Image[[:space:]]\+\([[:digit:]]\+\).*|\1|p' | \
	              sort -n | \
	              tail -n 1)"

	if [ "${_config_cnt}" -ne "$((_config_nodes - 1))" ]; then
		fail "Invalid number of Configuration node(s) in itb: $((_config_cnt + 1)) != ${_config_nodes}"
	fi

	if [ "${_image_cnt}" -ne "$((_image_nodes - 1))" ]; then
		fail "Invalid number Image node(s) in itb: $((_image_cnt + 1)) != ${_image_nodes}"
	fi
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*'/'}' test env"

	# Use a mkimage that supports configuration node signing, See issue #1
	if [ -f '/usr/local/bin/mkimage' ]; then
		echo 'Existing mkimage, cannot continue'
		exit 1
	fi
	cp "${FIXTURES}/mkimage" '/usr/local/bin/'

	echo
	echo '================================================================================'
}

oneTimeTearDown()
{
	if [ -f '/usr/local/bin/mkimage' ]; then
		unlink '/usr/local/bin/mkimage'
	fi
}

setUp()
{
	test_build_output="$(mktemp -d -p "${SHUNIT2_TMPDIR:-/tmp}" 'test_built_output.XXXXXX')"
	echo
}

tearDown()
{
	if [ -d "${test_build_output}" ]; then
		rm -f -r "${test_build_output:?}"
	fi
	echo "--------------------------------------------------------------------------------"
}

testBuildSimpleFitImage()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleFitImage" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Simple unsigned FitImage should have been built' "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleFitImage" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa"
	assertTrue 'Simple signed FitImage should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 3 1
}

testBuildSimpleImages()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleImages" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Simple unsigned Images should have been built' "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleImages" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa"
	assertTrue 'Simple signed Images should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 3 1
}

testBuildSimpleInitramFS()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleInitramFS" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Simple initramfs should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 3 1
}

testBuildSimpleRootFS()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleRootFS" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Simple rootfs should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 3 1
}

testBuildSimpleFitTemplate()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleFitTemplate" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}" \
	                        -t "${FIXTURES}/configs/simpleFitTemplate/fit_image.its.in"
	assertTrue 'Simple FitTemplate should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fit_image.itb" 2 1
}

testBuildFullImage()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/FullImage" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Full unsigned image should have been built' "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/FullImage" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa"
	assertTrue 'Full signed image should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 3 1
}

testBuildFullImageWithTemplate()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/UnsignedFullImageWithTemplate" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Full unsigned image with template should have been built' "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/SignedFullImageWithTemplate" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa"
	assertTrue 'Full signed image with template should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fit_image.itb" 3 1
}

testRootFSWithCustomKey()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/RootFSWithCustomKey" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/"
	assertTrue 'RootFS with custom key should have been built' "[ ${?} -eq 0 ]"
}

testFitImageWithExternalRepo()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/FitImageWithExternalRepo" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/"
	assertFalse 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/FitImageWithExternalRepo" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
				-u
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"
}

testFitImageWithKernel()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/FitImageWithKernel" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
	                        -u
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.cpio.itb" 4 2
}

testFitImageWithDTMatchInitramFS()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/FitImageDTMatchInitramFS" \
	                        -d "am335x-evbox-id.charger_v1.0" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
				-u
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"
}

testBuildRootFSWithKernelPackage()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/RootFSWithKernelPackage" \
	                        -d "am335x-evbox-id.charger_v1.0" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
	                        -u
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"
}

testBuildSimpleFPGA()
{
	"${COMMAND_UNDER_TEST}" \
	                        -b "${FIXTURES}/configs/simpleFPGA" \
	                        -d "${FIXTURES}/dummy.dtb" \
	                        -f "${FIXTURES}/fpga.bit" \
	                        -i "${FIXTURES}/Initramfs" \
	                        -k "${FIXTURES}/vmlinux" \
	                        -o "${test_build_output}"
	assertTrue 'Simple FPGA image should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.itb" 4 1
}

testBuildKitchenSink()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/KitchenSink" \
	                        -d "am335x-evbox-id.charger_v1.0" \
	                        -f "${FIXTURES}/fpga.bit" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa" \
				-x "gzip"
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.cpio.gz.itb" 6 2
}

testBuildDoubleKitchenSink()
{
	"${COMMAND_UNDER_TEST}" \
	                        -a "armv7" \
	                        -b "${FIXTURES}/configs/doubleKitchenSink" \
	                        -d "am335x-evbox-id.charger_v1.0" \
	                        -f "${FIXTURES}/fpga.bit" \
	                        -o "${test_build_output}" \
	                        -r ",${FIXTURES}/kernel_repo/" \
	                        -s "${FIXTURES}/test@esbs-5ffd9afb.rsa" \
				-x "lz4"
	assertTrue 'RootFS with kernel in fitImage should have been built' "[ ${?} -eq 0 ]"

	verify_itb "${test_build_output}/fitImage.cpio.lz4.itb" 6 2
	verify_itb "${test_build_output}/fitImage.setup.cpio.lz4.itb" 6 2
	verify_itb "${test_build_output}/fitImage.squashfs.lz4.itb" 6 2
}
